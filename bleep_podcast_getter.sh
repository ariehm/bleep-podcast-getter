#!/bin/bash

if [ ! -d "download" ]
then
	mkdir download
fi

wget http://podcast.bleep.com/weekly_podcast_itunes.xml -O weekly_podcast_itunes.xml

url=$(fgrep '<guid>' weekly_podcast_itunes.xml | egrep -oi 'http.+\.(mp3|m4a)')

for i in "${url[@]}"
do
	wget $i -P ./download/
done

printf "%s\n" "${url[@]}"
